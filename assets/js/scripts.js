$(window).on('load', function () {
    var responsetxt;
    var ytresponse;
    var srchkey;
    var ytsrchkeywrd;
    var ytsrchyr;
    var sbtrespnse;
    var sblink;
    var errresp;
    var tmp = '';
    var more_info;

    function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    };

    $(document).ready(function () {
        srchkey = getUrlParameter('searchtxt');
        get_srch_results(srchkey);

    });
function get_srch_results(s){
    $.ajax({
        url:'http://www.omdbapi.com/?s=' + s + '&apikey=f8fadf27',
        method:'GET',
        dataType:'json',
        error: function(jqXHR, exception){req_err(jqXHR,exception);},
        success: function(srchresults){
            $.each(srchresults.Search,function(key,value){
              tmp += '<div class="col s3 col-mrgns">';
              if(value.Poster=='N/A'){
                tmp +='<div class="sr-postr-cnt z-depth-3"><img src="assets/img/poster-na.jpg" alt=""></div>';   
              }else{tmp +='<div class="sr-postr-cnt z-depth-3"><img src="'+ value.Poster +'" alt=""></div>';}
              tmp +='<div class="sr-ttle">'+ value.Title +'&nbsp;('+ value.Type +')&nbsp;'+value.Year+'</div>';
              tmp+='<form action="response.html" method="GET">'
              tmp+='<input name="moreinfo" value='+ value.imdbID +' type="text" class="sr-rslts-inputbx">'
              tmp+='<button class="btn purple blue accent-4 waves-effect waves-light sr-rslts-btn" type="submit" name="action">More Info'
              tmp+='</button>'
              tmp+='</form>'; 
              tmp+='</div>';});
            $('#SrchResults').prepend(tmp);
        },
        complete: function () {
            setTimeout(function () {
                $(".g-pgloader-wr").remove();
            }, 1000)

        }
    });
}
$('#MoreInfo').ready(function(){
more_info = getUrlParameter('moreinfo');
getreq(more_info);
})
    function getreq(s) {
        $.ajax({
            url: 'http://www.omdbapi.com/?i=' + s + '&apikey=f8fadf27',
            method: 'GET',
            dataType: 'json',
            error: function(jqXHR, exception){req_err(jqXHR,exception);},
            success: function (jsondata) {
                responsetxt = jsondata;
                console.log(responsetxt.Actors);
                $('#PosterCnt').attr('src', responsetxt.Poster);
                $('#Mname').text(responsetxt.Title);
                $('#Yr').append(responsetxt.Year);
                $('#Genre').append(responsetxt.Genre);
                $('#runTime').append(responsetxt.Runtime);
                $('#cast').append(responsetxt.Actors);
                $('#StoryPlot').append(responsetxt.Plot);
                $('#Director').append(responsetxt.Director);
                $('#Awrds').append(responsetxt.Awards);
                $('#IMDBR').append(responsetxt.imdbRating);
                ytsrchkeywrd = responsetxt.Title;
                ytsrchkeywrd = ytsrchkeywrd.replace(/ /g, "+");
                ytsrchyr = '+' + responsetxt.Year + '+';
                ytsrchkeywrd += ytsrchyr;
                sbtrespnse = responsetxt.imdbID;
                getytID(ytsrchkeywrd);
                getsbtle(sbtrespnse);
            }
            
        });

        function getytID(ys) {
            $.ajax({
                url: 'https://www.googleapis.com/youtube/v3/search?part=snippet&q=' + ys + 'official+trailer' + '&type=video&key=AIzaSyDmQD93g4_fdlyhsi_rO3jDrmQBsb8NMaw',
                method: 'GET',
                dataType: 'json',
                error: function(jqXHR, exception){req_err(jqXHR,exception);},
                success: function (jsondata) {
                    ytresponse = jsondata.items[0].id.videoId;
                    $('#Mtrailer').attr('src', 'https://www.youtube.com/embed/' + ytresponse);

                },
                complete: function () {
                    setTimeout(function () {
                        $(".g-pgloader-wr").remove();
                    }, 1000)

                }
            });
        }
          function getsbtle(sid) {
            $.ajax({
                url: 'https://subtitle-api.org/videos/' + sid + '/subtitles?Language%20of%20the%20subtitles%20you%20want%20to%20retrieve=english&Number%20of%20subtitles%20to%20retrieve=20',
                method: 'GET',
                dataType: 'json',
                error: function(jqXHR, exception){req_err(jqXHR,exception);},
                success: function (jsondata) {
                    errresp = jsondata.best;
                    if (errresp == null) {
                        $('#subsCnt').css('display', 'none');
                    } else {
                        sblink = jsondata.items[0].downloadPage;
                        $('#subTitleDwnlnk').attr('href', sblink);
                    }
                },
            });
        }
    }
    function req_err(err, ex){
        if (jqXHR.status === 0) {
            alert('Not connect.\n Verify Network.');
        } else if (err.status == 404) {
            alert('Requested page not found. [404]');
        } else if (err.status == 500) {
            alert('Internal Server Error [500].');
        } else if (ex === 'parsererror') {
            alert('Requested JSON parse failed.');
        } else if (ex === 'timeout') {
            alert('Time out error.');
        } else if (ex === 'abort') {
            alert('Ajax request aborted.');
        } else {
            alert('Uncaught Error.\n' + err.responseText);
        }
    }
});